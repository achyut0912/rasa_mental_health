## happy path
* greet
  - utter_greet
* mood_happy
  - utter_happy

## sad path 1
* greet
  - utter_greet
* mood_unhappy
  - utter_cheer_up
  - utter_did_that_help
* affirm
  - utter_happy

## sad path 2
* greet
  - utter_greet
* mood_unhappy
  - utter_cheer_up
  - utter_did_that_help
* deny
  - utter_ask_song
* deny
  - utter_suggest_doctor
* affirm
  - utter_goodbye
  
## sad path 3
* greet
  - utter_greet
* mood_unhappy
  - utter_ask_mood
* mood_unhappy
  - utter_cheer_up
  - utter_did_that_help
* deny
  - utter_ask_song
* affirm
  - action_play_song
  - utter_did_that_help
* affirm
  - utter_goodbye
  
## sad path 3
* greet
  - utter_greet
* mood_unhappy
  - utter_ask_mood
* mood_unhappy
  - utter_cheer_up
  - utter_did_that_help
* deny
  - utter_ask_song
* affirm
  - action_play_song
  - utter_did_that_help
* deny
  - utter_suggest_doctor
* affirm
  - utter_goodbye
  
## anxiety 1
* greet
  - utter_greet
* mood_unhappy
  - utter_ask_mood
* mood_anxiety
  - utter_ask_reason
* anxiety_inform
  - utter_deny_anxiety
* thanks
  - utter_goodbye
  
## anxiety 2
* greet
  - utter_greet
* mood_anxiety
  - utter_ask_reason
* anxiety_inform
  - action_play_song
  - utter_did_that_help
* deny
  - utter_suggest_doctor
* affirm
  - utter_goodbye


## say goodbye
* goodbye
  - utter_goodbye

## bot challenge
* bot_challenge
  - utter_iamabot
