## intent:greet
- hey
- hello
- hi
- good morning
- good evening
- hey there

## intent:goodbye
- bye
- goodbye
- see you around
- see you later

## intent:affirm
- yes
- indeed
- of course
- Haan
- Zaroor
- Ye sahi hai
- Ye acha lag raha hai
- ye thik hai
- that sounds good
- correct

## intent:deny
- no
- never
- I don't think so
- don't like that
- no way
- not really
- nahi
- kabhi nahi
- ye acha nahi hai
- koi dusra batao
- isse kaam nahi hoga
- ye thik nahi hai
- ye nahi chalega

## intent:mood_happy
- perfect
- very good
- great
- amazing
- wonderful
- I am feeling very good
- I am great
- I'm good
- Me khush hun
- Bahut khushi ho rahi hai
- Acha lag raha hai
- aaj mood bahut acha hai
- mood acha hai
- Ab mujhe acha mehsoos ho raha h
- Acha mehsoos ho raha h

## intent:mood_unhappy
- sad
- very sad
- unhappy
- bad
- very bad
- awful
- terrible
- not very good
- extremely sad
- so sad
- Thoda dukhi hun
- me bahut dukhi hun
- dukh ho raha hain
- mood kharab hai
- mood thik nahi hai
- mood acha nahi hai
- aaj mood kharab hai 
- thoda mood kharab lag raha hai
- Acha nahi lag raha

## intent:mood_anxiety
- Chinta ho rahi hai
- Mujhe bahut chinta ho rahi hai
- Me bahut bachain hun
- Mujhe bachaini ho rahi hai
- Kuch bura hone wala hai
- Me ye nahi kar paunga
- Mujhe ajeeb lag raha hai
- Mera aatmvishwas khatam ho gaya hai
- Mujhe khud pe bharosa nahi hai
- Bahut bure sapne aa rahe hai
- me papers nahi de paunga
- Mujhe nahi hoga

## intent:anxiety_inform
- mujhe [paper](anxiety_reason) ka dar lag raha
- mujhe [kaam](anxiety_reason) ki chinta hai
- [kaam](anxiety_reason) ki wajah se
- [papers](anxiety_reason) ki wajah se 
- [papers](anxiety_reason) ki chinta hai bahut
- [papers](anxiety_reason)
- mujhe [papers](anxiety_reason) ka dar lag raha hai

## intent:mood_fear
- Mujhe dar lag raha hai
- Me wahan nahi jaunga
- Wahan koi hai
- Maine darawana sapna dekha
- Me bahut dar gaya 

## intent:bot_challenge
- are you a bot?
- are you a human?
- am I talking to a bot?
- am I talking to a human?
